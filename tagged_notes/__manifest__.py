{
    "name": "Tagged Notes",
    "version": "13.0.1.0.0",
    'description': "Tagged notes support",
    "author": "shallow.droid",
    "license": "AGPL-3",
    "depends": ["base", "project"],
    "application": False,
    "installable": True,
}