{
    'name': "Single Page Application Support",
    "version": "13.0.1.0.0",
    'description': "Single Page Application Support.",
    'author': "shallow.droid",
    "license": "AGPL-3",
    'depends': ["base"],
    "application": False,
    "installable": True,
}
