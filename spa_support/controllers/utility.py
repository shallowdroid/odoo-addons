# -*- coding: utf-8 -*-
from odoo import http, _
from odoo.http import request, Response
import logging

_logger = logging.getLogger(__name__)


class Utility(http.Controller):

    @http.route('/spa-support/utility/health', auth="none", type="http", cors="*", methods=['GET'])
    def get_health(self):
        # todo: add checks for file system, database, etc
        return Response("OK", status=200)
